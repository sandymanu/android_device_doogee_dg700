## Specify phone tech before including full_phone
$(call inherit-product, vendor/cm/config/gsm.mk)

# Release name
PRODUCT_RELEASE_NAME := dg700

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/doogee/dg700/device_dg700.mk)

## Device identifier. This must come after all inclusions
PRODUCT_DEVICE := dg700
PRODUCT_NAME := cm_dg700
PRODUCT_BRAND := doogee
PRODUCT_MODEL := dg700
PRODUCT_MANUFACTURER := doogee

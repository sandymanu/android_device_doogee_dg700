$(call inherit-product, $(SRC_TARGET_DIR)/product/languages_full.mk)

# The gps config appropriate for this device
$(call inherit-product, device/common/gps/gps_us_supl.mk)

$(call inherit-product-if-exists, vendor/doogee/dg700/dg700-vendor.mk)

PRODUCT_CHARACTERISTICS := nosdcard

DEVICE_PACKAGE_OVERLAYS += device/doogee/dg700/overlay/

LOCAL_PATH := device/doogee/dg700
ifeq ($(TARGET_PREBUILT_KERNEL),)
	LOCAL_KERNEL := $(LOCAL_PATH)/kernel
else
	LOCAL_KERNEL := $(TARGET_PREBUILT_KERNEL)
endif

PRODUCT_PACKAGES += \
    libxlog

PRODUCT_PACKAGES += \
    lights.mt6582
	
PRODUCT_PACKAGES += \
	libmtkrilw

PRODUCT_PACKAGES += \
	audio.r_submix.default

PRODUCT_PACKAGES += \
	audio.primary.mt6582

PRODUCT_PACKAGES += \
	audio_policy.default

PRODUCT_PACKAGES += \
    lib_driver_cmd_mtk

PRODUCT_PACKAGES += \
        libblisrc

PRODUCT_COPY_FILES += \
    device/doogee/dg700/mtk-kpd.kl:system/usr/keylayout/mtk-kpd.kl

PRODUCT_COPY_FILES += \
    device/doogee/dg700/init.recovery.mt6582.rc:root/init.recovery.mt6582.rc

PRODUCT_COPY_FILES += \
    device/doogee/dg700/fstab.mt6582:root/fstab.mt6582 \
    device/doogee/dg700/init.mt6582.rc:root/init.mt6582.rc \
	device/doogee/dg700/init.mt6582.usb.rc:root/init.mt6582.usb.rc \
	device/doogee/dg700/init.modem.rc:root/init.modem.rc \
	device/doogee/dg700/init.protect.rc:root/init.protect.rc \
    $(LOCAL_KERNEL):kernel
		
PRODUCT_COPY_FILES += \
	frameworks/native/data/etc/handheld_core_hardware.xml:system/etc/permissions/handheld_core_hardware.xml \
	frameworks/native/data/etc/android.hardware.wifi.direct.xml:system/etc/permissions/android.hardware.wifi.direct.xml \
	frameworks/native/data/etc/android.hardware.wifi.xml:system/etc/permissions/android.hardware.wifi.xml \
	frameworks/native/data/etc/android.hardware.telephony.gsm.xml:system/etc/permissions/android.hardware.telephony.gsm.xml \
	frameworks/native/data/etc/android.hardware.sensor.light.xml:system/etc/permissions/android.hardware.sensor.light.xml \
	frameworks/native/data/etc/android.hardware.sensor.proximity.xml:system/etc/permissions/android.hardware.sensor.proximity.xml \
	frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	frameworks/native/data/etc/android.hardware.camera.flash-autofocus.xml:system/etc/permissions/android.hardware.camera.flash-autofocus.xml \
	frameworks/native/data/etc/android.hardware.camera.front.xml:system/etc/permissions/android.hardware.camera.front.xml \
	frameworks/native/data/etc/android.hardware.camera.xml:system/etc/permissions/android.hardware.camera.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.distinct.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.distinct.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.multitouch.jazzhand.xml:system/etc/permissions/android.hardware.touchscreen.multitouch.jazzhand.xml \
	frameworks/native/data/etc/android.hardware.touchscreen.xml:system/etc/permissions/android.hardware.touchscreen.xml \
	frameworks/native/data/etc/android.hardware.bluetooth_le.xml:system/etc/permissions/android.hardware.bluetooth_le.xml \
    frameworks/native/data/etc/android.hardware.bluetooth.xml:system/etc/permissions/android.hardware.bluetooth.xml \
    frameworks/native/data/etc/android.hardware.faketouch.xml:system/etc/permissions/android.hardware.faketouch.xml \
    frameworks/native/data/etc/android.hardware.location.gps.xml:system/etc/permissions/android.hardware.location.gps.xml \
	device/doogee/dg700/android.hardware.microphone.xml:system/etc/permissions/android.hardware.microphone.xml \
	frameworks/native/data/etc/android.hardware.sensor.accelerometer.xml:system/etc/permissions/android.hardware.sensor.accelerometer.xml \
    frameworks/native/data/etc/android.hardware.sensor.compass.xml:system/etc/permissions/android.hardware.sensor.compass.xml
	
PRODUCT_COPY_FILES += \
	device/doogee/dg700/media_profiles.xml:system/etc/media_profiles.xml \
    device/doogee/dg700/media_codecs.xml:system/etc/media_codecs.xml \
    device/doogee/dg700/audio_policy.conf:system/etc/audio_policy.conf \
    device/doogee/dg700/audio_effects.conf:system/etc/audio_effects.conf
	
PRODUCT_PACKAGES += \
Torch	

$(call inherit-product, build/target/product/full.mk)

PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0
PRODUCT_NAME := full_dg700
PRODUCT_DEVICE := dg700

$(call inherit-product, frameworks/native/build/phone-xhdpi-2048-dalvik-heap.mk)